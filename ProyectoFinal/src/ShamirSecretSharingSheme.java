/**
*Clase que implementa el Esquema de Shamir de Secreto Compartido
*Ayudado por las clases GeneradorPolinomio, Polinomio y Campo
*@author Rossana Palma Lopez
*@version 1.0
*/
import java.util.Scanner;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.Vector;
import java.math.BigInteger;
import java.io.Console;
import java.io.FileOutputStream;
import java.io.Writer;
import java.io.FileWriter;
import java.io.FileInputStream;
import java.io.DataInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.CipherInputStream;
import java.io.IOException;
@SuppressWarnings("unchecked")

public class ShamirSecretSharingSheme {
    
    protected static int n, t;
    private static Campo c= new Campo();
    protected static boolean encriptar, desencriptar;
    protected static String evalEncripArch, evalDesCripArch, archivoClaro, archEncrip;
    
    /**
    *Constructor por defecto de la clase
    */
    public ShamirSecretSharingSheme() {
		try {
	    	this.n = -1;
	    	this.t = -1;
	    	this.encriptar = false;
	    	this.desencriptar = false;
	    	this.evalEncripArch = "evaluaciones.frg";
	    	this.evalDesCripArch = "evaluaciones.frg";
		} catch(Exception e) {
	    	System.err.println(e);
	    	System.exit(1);
		}
    }

    /**
    *Metodo que crea el archivo con las n evaluaciones del Polinomio de grado t-1
    *@param evaluaciones vector que contiene el punto donde fue evaluado el Polinomio
    *y su evaluacion correspondiente
    */
    public void crearArchEvals(Vector[] evaluaciones) {
		Writer wr;
		try {
	    	wr = new FileWriter(evalEncripArch);
	    	for(int i = 0; i < evaluaciones.length; i++){
	    		wr.write(evaluaciones[i].elementAt(0).toString()+","+evaluaciones[i].elementAt(1).toString()+"\n");		
	    	}
			wr.close();
	    	System.out.println("Creacion de archivo: Correcto");
		} catch(Exception e) {
	    	System.err.println(e);
	    	System.exit(1);
		}
    }

    /**
    *Metodo que encripta un archivo dado utilizando la llave
    *generada atravez de una contraseña ingresada y representada
    *como un arreglo de tipo byte
    *Ademas se utiliza el algoritmo AES para encriptar
    *@param contraseña un arreglo de tipo byte que representa la contraseña
    */
    public void encriptar(byte[] contraseña) {
		int toWrite;
		String nombreArchivo;
		CipherInputStream ci;
		FileOutputStream wr; 
		SecretKeySpec sec;
		Cipher cipher;
		try {
	    	if(archivoClaro.lastIndexOf('.') == -1){
	    		nombreArchivo = archivoClaro + ".aes";	
	    	}
		
	    	else{
	    		nombreArchivo = archivoClaro.substring(0,archivoClaro.lastIndexOf('.'))+".aes";	
	    	}
		
		    cipher = Cipher.getInstance("AES");
	    	wr = new FileOutputStream(nombreArchivo,true);
	    	sec = new SecretKeySpec(contraseña,"AES");
	    	cipher.init(Cipher.ENCRYPT_MODE,sec);
	    	ci = new CipherInputStream(new FileInputStream(archivoClaro),cipher);
	    	while((toWrite=ci.read()) != -1){
	    		wr.write(toWrite);
	    	}
				
	    	wr.close();
	    	ci.close();
	    	System.out.println("Encriptacion de archivo: Correcto");
		} 
		catch(Exception e) {
	    	System.err.println(e);
	    	System.exit(1);
		}
	}

	/**
	*Metodo que devuelve la contraseña en forma de un arreglo de tipo byte
	*Se utiliza algoritmo SHA-256 para generar una representacion segura de la contrseña
	*lo que depsiues dera nuestra clave
	*@return un arreglo de tipo byte que representa la contraseña dada
	*/
    public byte[] obtenerContraseña() {
		char[] contraseña=null;
		byte[] bytes = null;
		Console cons;
		MessageDigest md;
		try {
	    	if ((cons=System.console())!=null && (contraseña=cons.readPassword("[%s]","Ingresa una contraseña: "))!=null) {
				md = MessageDigest.getInstance("SHA-256");
				bytes = md.digest(new String(contraseña).getBytes());
				Arrays.fill(contraseña, ' ');
	    	}
		} 
		catch(Exception e) {
	    	System.err.println(e);
	    	System.exit(1);
		}
		return bytes;
    }
   
   /**
   *Metodo que desencripta un archivo que ha sido encriptado previamente
   *es decir devuelve la llave (contraseña) reprsentada como 
   *un arreglo de tipo byte
   *Utliza la interpolacion de Lagrange para obtener devuelta la llave 
   *(termino independiente, en el Polinomio)
   *@return la contraseña oculta para desencriptar el archivo
   */
    public byte[] getLlaveK() {
	Vector[] arr = null;
	byte [] arr1 = null;
	MessageDigest md;
	LinkedList<Vector> list = new LinkedList<Vector>();
	Polinomio p = new Polinomio();
       	try {
            String line;
            FileInputStream fstream = new FileInputStream(evalDesCripArch);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
	   		System.out.println("Leyendo las evaluaciones...");
            while((line=br.readLine()) != null) {
				list.add(new Vector(2));
				((Vector)list.getLast()).add(0,new BigInteger(line.substring(0,line.indexOf(','))));
				((Vector)list.getLast()).add(1,new BigInteger(line.substring(line.indexOf(',')+1,line.length())));
            }
	    	in.close();
	    	arr = new Vector[list.size()];
	    	for(int i = 0; i < arr.length; i++){
				arr[i] = (Vector)list.get(i);
	    	}
	    	System.out.println("Evaluando con el metodo de Larange...");
	    	return p.interpolLagrange(new BigInteger("0"),arr).toByteArray();
        } 
        catch (Exception e) {
            System.err.println(e);
            System.exit(1);
		}
		return arr1;

    }

	/**
	*Metodo que re-crea el archivo que ha sido encriptado
	*Utiliza el algoritmo AES para desencriptar y la contraseña
	*@param contraseña arreglo de tipo byte que representa nuestra contraseña
	*/	
    public void crearArchDesEncrip(byte[] contraseña) {
		int toWrite;
		String nombreArchivo;
		CipherInputStream ci;
		FileOutputStream wr; 
		SecretKeySpec sec;
		Cipher cipher;
		try {
	    	if(archEncrip.lastIndexOf('.') == -1){
	    		nombreArchivo = archEncrip;	
	    	}
		
	    	else{
	    		nombreArchivo = archEncrip.substring(0,archEncrip.lastIndexOf('.'));	
	    	}
		
	    	cipher = Cipher.getInstance("AES");
	    	wr = new FileOutputStream(nombreArchivo);
	    	sec = new SecretKeySpec(contraseña,"AES");
	    	cipher.init(Cipher.DECRYPT_MODE,sec);
	    	ci = new CipherInputStream(new FileInputStream(archEncrip),cipher);
	    	while((toWrite=ci.read()) != -1){
	    		wr.write(toWrite);
	    	}
				
	    	wr.close();
	    	ci.close();
	    	System.out.println("Archivo desencriptado: Correcto");
		} 
		catch(Exception e) {
	    	System.err.println(e);
	    	System.exit(1);
		}
    }

    /**
    *Metdo Main de la clase
    *@param args arreglo que contiene todos los argumentos
    *a ejecutar
    */
    public static void main(String[] args) {
		GeneradorPolinomio gP = new GeneradorPolinomio();
		ShamirSecretSharingSheme sh = new ShamirSecretSharingSheme();
		Scanner inI = new Scanner(System.in);
		Scanner inL = new Scanner(System.in);
		int opcion;
		System.out.println("Menu: \n 1.Encriptar \n 2.Desencriptar \n 3. Salir");
		opcion=inI.nextInt();
		switch(opcion){
			case 1:
				byte[] contraseña;
				encriptar = true;
				System.out.println("Nombre del archivo donde se guardaran las evaluaciones");
				evalEncripArch=inL.nextLine();
				System.out.println("Numero de evaluaciones requeridas");
				t=inI.nextInt();
				System.out.println("Numero minimo de puntos necesarios para desencriptar");
				n=inI.nextInt();
				System.out.println("Nombre archivo a encriptar");
				archivoClaro=inL.nextLine();
				contraseña=new BigInteger(sh.obtenerContraseña()).abs().toByteArray();
				sh.encriptar(contraseña);
				sh.crearArchEvals(gP.nPtObtenidos(new BigInteger(contraseña).mod(c.getPrimo()),t,n));
			break;
			case 2:
				desencriptar=true;
				System.out.println("Nombre del archivo con las evaluaciones requeridas");
				evalDesCripArch=inL.nextLine();
				System.out.println("Nombre de archivo a desencriptar");
				archEncrip=inL.nextLine();
				contraseña = sh.getLlaveK();
				sh.crearArchDesEncrip(contraseña);		
			break;
			case 3:
				System.exit(0);
			break;
			default :
				System.err.println("Ha ocurrido un error. No se ha indicado alguna accion");
	    		System.exit(1);
			break;					
		}
	}	
}
//Fin de la clase ShamirSecretSharingSheme.java
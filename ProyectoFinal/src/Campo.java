/**
*Clase que crea un Campo entero para realizar sobre
*de el operaciones matematicas
*@author Rossana
*@version 1.0
*/
import java.math.BigInteger;
public class Campo{
	private BigInteger primoElegido= new BigInteger("208351617316091241234326746312124448251235562226470491514186331217050270460481");

	/**
	*Constructor por defecto de la clase
	*/
	public Campo(){}

	/**
	*Constructor de la clase apartir de un string
	*que representa un numero primo que representa
	*el campo numerico p
	*@param primoX string que reprsenta p de Zp
	*/
	public Campo(String primoX){
		setPrimo(primoX);
	}

	/**
	*Setter que modifica el p del Zp
	*@param nuevoPrimo
	*/
	public void setPrimo(String nuevoPrimo){
		primoElegido=new BigInteger(nuevoPrimo);
	}

	/**
	*Getter que modifica el p del Zp
	*@return entero que representa el p del Zp
	*/
	public BigInteger getPrimo(){
		return primoElegido;
	}
}
//Fin de la clase Campo.java
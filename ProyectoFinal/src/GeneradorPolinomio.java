/**
*Clase que genera el Polinomio del Esquema de Shamir
*del Secreto Compartido
*@author Rossana Palma Lopez
*@version 1.0
*/
import java.util.Vector;
import java.math.BigInteger;
import java.util.Random;
@SuppressWarnings("unchecked")
public class GeneradorPolinomio{
	
	Campo c= new Campo();//Objeto de la clase Campo

	/**
	*Metodo que devuelve un arreglo de vectores
	*los cuales contienen un punto y la evaluacion de
	*algun Polinomio en este
	*@param termIndp BigInteger que representa el termino
	*independiente del polinomio es decir nuestra clave
	*@param t representa el grado de nuestro polinomio
	*@param n representa el numero minimo que se necesitara 
	*para desencriptar el numero de evaluaciones
	*@return un arreglo de vectores que contiene los puntos de 
	*las n evaluaciones
	*/
	public  Vector [] nPtObtenidos (BigInteger termIndp, int tPt, int numEvals){
		return obtenerEvals(constPolinomio(termIndp.mod(c.getPrimo()),tPt),numEvals);
	}
	
	/**
	*Metodo que crea el Polinomio de BigIntegers 
	*con el grado t indicado 
	*@param termIndp BigInteger que representa el termino
	*independiente del polinomio es decir nuestra clave
	*@param grado el grado t del polinomio
	*@return el Polinomio formado con los coeficientes 
	*de tipo BigInteger generados aleatoriamente
	*/
	public Polinomio constPolinomio(BigInteger termIndp, int grado){
		BigInteger coef=null;
		BigInteger [] arrCoefs = new BigInteger[grado];
		arrCoefs[0]=termIndp;
		for(int i=1; i<grado; i++){
			do{
				coef=new BigInteger(c.getPrimo().bitLength(),new Random());		
			}
			while(coef.compareTo(c.getPrimo())>=0||coef.compareTo(BigInteger.ZERO)==0);
			arrCoefs[i]=coef;
		}
		Polinomio p= new Polinomio(arrCoefs);
		return p;
	}


	/**
	*Metodo que devuelve las evaluaciones del Polinomio 
	*en los n puntos aleatorios, estas evaluaciones en un arreglo de
	*vectores
	*@param p Polinomio generado con los coeficientes aleatorios
	*@param numEval numero de evaluaciones
	*@return un arreglo de vectores donde se guardan las n evaluaciones
	*/
	public Vector [] obtenerEvals(Polinomio p, int numEvals){

		BigInteger x=null;
		BigInteger result=new BigInteger("0");
		Vector [] vals=new Vector[numEvals];
		for(int i=0; i<vals.length; i++){
			do{
				x=new BigInteger(c.getPrimo().bitLength(),new Random());

			}
			while(x.compareTo(c.getPrimo())>=0||x.compareTo(BigInteger.ZERO)==0);
			vals[i]=new Vector(2);
			vals[i].add(0,x);
			result=result.add(p.evaluar(x));
			vals[i].add(1,result);
			result=new BigInteger("0");

		}
		return vals;
		


	}
}//Fin de la clase GeneradorPolinomio.java
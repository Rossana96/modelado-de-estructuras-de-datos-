/**
*TDA de Polinomio
*Clase que implemente el TDA Polinomio
*La clase crea Polinomios del tipo BigInteger
*En esta clase se contienen las operaciones escenciales
*quen se pueden realizar sobre un polinomio. Es decir
*Suma, Multiplicacion y Resta de Polinomios, ademas del
* metodo evaluar un polinomio en un BigInteger
*y el metodo de Lagrange para BigIntgers
*@author Rossana Palma Lopez
*@Version 1.0 
*/
import java.math.BigInteger;
import java.util.Vector;
@SuppressWarnings("unchecked")
public class Polinomio{
	private BigInteger [] coefs;
	private int grado;
	private Campo c= new Campo();

	/**
	*Contructor vacio de la clase
	*/
	public Polinomio(){}

	/**
	*Constructor de un Polinomio apartir de un 
	*coeficiente de tipo BigInteger y el grado
	*@param coef Coeficiente del polinomio(monomio)
	*@param gr grado del monomio
	*/
	public Polinomio(BigInteger coef, int gr){
		coefs=new BigInteger[gr+1];
		coefs[gr]=coef;
		grado=getGrado();
	}	

	/**
	*Constructor de un Polinomio apartir de un
	*arreglo de coeficientes de tipo BigInteger 
	*@param cfs arreglo de tipo BigInteger de Coeficientes 
	*/
	public Polinomio(BigInteger [] cfs){
		coefs=cfs;
		grado=getGrado();
	}

	/**
	*getter de la clase
	*Obtiene el grado del Polinomio
	*con el tamaño del arreglo de coeficientes menos 1
	*@return un entero que representa el grado
	*/
	public int getGrado(){
		return coefs.length-1;
	}

	/**
	*Metodo que calcula el mayor de dos numeros enteros
	*@param a entero que representa el primer numero
	*@param b entero que reprsenta al segundo numero
	*@return entero que reprsenta el mayor de 2 numeros
	*de los parametros
	*/
	public int mayor(int a, int b){
		int mayor=0;
		if(a<b){
			mayor=b;
		}
		else{
			mayor=a;
		}
		return mayor;
	}
	
	/**
	*Metodo que calcula la suma de dos Polinomios
	*devolviendo el Polinomio resultante.
	*Aplicamos en metdo correspondiente de BigInteger para la suma
	*@param p Polinomio a sumar con el que invoca el metodo
	*@retrun p Polinomio resultante al aplicar la operacion
	*/
	public Polinomio suma(Polinomio p){
		Polinomio aux= new Polinomio(BigInteger.ZERO,mayor(grado,p.grado));
		for (int i=0; i<=grado; i++) {
			aux.coefs[i].add(coefs[i]).mod(c.getPrimo());

		}
		for (int j=0; j<=p.grado; j++) {
			aux.coefs[j].add(p.coefs[j]).mod(c.getPrimo());
		}
		aux.grado=aux.getGrado();
		return aux;
	}

	/**
	*Metodo que calcula la resta de dos Polinomios
	*devolviendo el Polinomio resultante.
	*Aplicamos en metdo correspondiente de BigInteger para la resta
	*@param p Polinomio a restar con el que invoca el metodo
	*@retrun p Polinomio resultante al aplicar la operacion
	*/
	public Polinomio resta(Polinomio p){
		Polinomio aux= new Polinomio(BigInteger.ZERO,mayor(grado,p.grado));
		for (int i=0; i<=grado; i++) {
			aux.coefs[i].add(coefs[i]);

		}
		for (int j=0; j<=p.grado; j++) {
			aux.coefs[j].subtract(p.coefs[j]);
		}
		aux.grado=aux.getGrado();
		return aux;	
	}

	/**
	*Metodo que calcula la multiplicacion de dos Polinomios
	*devolviendo el Polinomio resultante.
	*Aplicamos en metdo correspondiente de BigInteger para la multiplicacion
	*@param p Polinomio a multiplicar con el que invoca el metodo
	*@retrun p Polinomio resultante al aplicar la operacion
	*/
	public Polinomio producto(Polinomio p){
		Polinomio aux= new Polinomio(BigInteger.ZERO,grado+p.grado);
		for (int i=0; i<=grado; i++) {
			aux.coefs[i].add(coefs[i]);
			for (int j=0; j<=p.grado; j++) {
				aux.coefs[i+j].add((coefs[i].multiply(p.coefs[j])).mod(c.getPrimo())).mod(c.getPrimo());
			}
		}
		aux.grado=aux.getGrado();
		return aux;	
	}

	/**
	*Metodo que evalua al Polinomio en un BigInteger dado en los paremtros
	*@param a BigInteger con el que sera evaluado el Polinomio
	*@return BigInteger que resulta de evaluar el Polinomio en a 
	*/
	public BigInteger evaluar(BigInteger a){
		BigInteger result=new BigInteger("0");
		for(int i=0; i<grado; i++){
			result.add((a.modPow(BigInteger.valueOf((long)i),c.getPrimo())).multiply(this.coefs[i]).mod(c.getPrimo())).mod(c.getPrimo());
		}
		return result;
	}

	/**
	*Metodo que aplica la interpolacion polinomica de Lagrange
	*es decir evalua un polinomio con el metodo de Lagrange
	*@param bI Valor a evaluar el polinomio
	*@param pts un vector con los puntos obtenidos y su evaluciones
	*@return un BigInteger que representa el valor independiente o llave
	*/
	public BigInteger interpolLagrange(BigInteger bI, Vector [] pts){
		BigInteger llave =new BigInteger("0");
		BigInteger numdr=new BigInteger("1");
		BigInteger denom=new BigInteger("1");
		BigInteger result=null;
		BigInteger aux=null;
		BigInteger temp=null;
		for(int i=0; i<pts.length; i++){
			numdr=new BigInteger("1");
			denom=new BigInteger("1");
			for(int j=0; j<pts.length; j++){
				if(j!=i){
					temp=(BigInteger)pts[i].elementAt(0);
					aux=(BigInteger)pts[j].elementAt(0);
					numdr=numdr.multiply(bI.subtract(aux).mod(c.getPrimo())).mod(c.getPrimo());
					denom=denom.multiply(temp.subtract(aux).mod(c.getPrimo())).mod(c.getPrimo());
				}
			}
			result=numdr.multiply(denom.modInverse(c.getPrimo())).mod(c.getPrimo());
			llave=llave.add((BigInteger)pts[i].elementAt(1)).multiply(result).mod(c.getPrimo()).mod(c.getPrimo());
		}

		return llave;
	}
}
//Fin de la clase Polinomio.java
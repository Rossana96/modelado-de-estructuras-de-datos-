import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
public class Mostrar extends LSB{

	private BufferedImage img;

	public void decodificar(String nameImg, String nameTxtResult){
		try{
			img = ImageIO.read(new File(nameImg));
            int alto= img.getHeight();
            int ancho=img.getWidth();
            char c=' ';
            int nPixel;
            for(int i=0; i<(ancho*alto)-3; i=i+3){
            	nPixel=i;
            	for(int j=0; j<8; j=j+3){
            		int y=(int)(nPixel/ancho);
            		int x=nPixel-(y*ancho);
            		int colorPixel=img.getRGB(x,y);
            		Color color=new Color(nPixel);
            		int red=color.getRed();
            		int green=color.getGreen();
            		int blue=color.getBlue();
            		if(j==0||j==3||j==6){
            			c=mostarLSB(red,j);
            		}
            		else if(j==1||j==4||j==7){
            			c=mostarLSB(green,j);
            		}
            		else if(j==2||j==5){
            			c=mostarLSB(blue,j);
            		}
            		nPixel++;
            	}
				FileWriter fw=new FileWriter(nameTxtResult);
		        escribirTxt(fw,c);			
	        
            }
            
		}
		catch (IOException ex) {
            		System.out.println("No se pudo leer la imagen");
        	}

	}
	
	private static void escribirTxt(FileWriter fw, char c){
		try{
			fw.write(c);
		}
		catch(IOException e){
			System.out.println("Error al escribir el archivo");
		}
		finally{
		    try{
		        if(fw!=null){
				    fw.close();
			    }    
		    }
		    catch(IOException e){
		    	System.out.println("Error al cerrar el archivo");
		    }
			
		}
		
	}
}
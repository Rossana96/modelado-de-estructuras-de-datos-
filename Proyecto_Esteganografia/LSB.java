/**
 * Clase que implementa el metodo LSB para cambiar bits de archivos
 * Utilizando operaciones a anivel de bits (Bitwise)
 * @author Rossana Palma López
 * @version 1.0
 * */
public class LSB{

    /**
     * Metodo que cambia el ultimo bit de cada byte que forma algun archivo
     * @param cambio entero que representa el bit que se guardara el la ultima posicion
     * @param aCambiar entero que representa el bit a cambiar
     * @param posicion entero que representa la posicion del bit que se cambiara
     * */
	public void cambiarLSB(int cambio, int aCambiar, int posicion){
		if(tieneBits((byte)cambio,posicion)){
			aplicarLSB1(aCambiar,0);
		}
		else{
			aplicarLSB2(aCambiar,0);
		}
	}

    /**
     * Metodo que devuelve el caracter correspondiente a un conjunto de bits
     * utilizando el metodo de cambio LSB
     * @param byteX entero que representa el byte a revisar para extarer un bit del caracter
     * @param posicion entero que representa la posicion que le corresponde al bit en el caracter 
     * @return caracter formado por el bit estraido del byte dado
     * */
	public char mostarLSB(int byteX,int posicion){
		char c=' ';
		if(tieneBits((byte)byteX,0)){
			aplicarLSB1(c,posicion);

		}
		else{
			aplicarLSB2(c,posicion);
		}
		return c;

	}
    
    /**
     * Metodo estatico que realiza el metodo LSB 
     * cuando el ultimo bit del byte a comparar es 
     * */
	 private static void aplicarLSB1(int byteX,int pos){
        byteX = byteX | 1<<pos;	
         
    }

    private static void aplicarLSB2(int byteX, int pos){
        byteX = byteX & (~(1<<pos)); 
    }

	private static boolean tieneBits(byte num,int bit){
		return ((byte)num&(1<<bit))!=0;
	}
}
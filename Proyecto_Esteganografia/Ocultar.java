import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.lang.StringBuilder;
public class Ocultar extends LSB{
	
	private BufferedImage img;
	private BufferedReader txt;
	private FileReader f;
	private static int tamTxt;

	public void codificar(String imgNameOrigin, String txtName, String imgNameResult){
		try {
       		String txt = obtenerTxt(txtName);
            img = ImageIO.read(new File(imgNameOrigin));
            int alto= img.getHeight();
            int ancho=img.getWidth();
            int nPixel=0;
            if(txt.length()>((alto*ancho)/3)){
        	   System.out.println("El texto es mas grande que la imagen");
			   System.exit(0);        
            }
            for(int i=0; i<txt.length(); i++){
        	   for(int j=0; j<8; j=j+3){
        		    int y=(int)nPixel/ancho;
                    int x=nPixel-(y*ancho);
        			int colorPixel=img.getRGB(x,y);
        			Color c = new Color(colorPixel);
        		    int red=c.getRed();
        			int green=c.getGreen();
        			int blue=c.getBlue();
                    int k=j/3;
                    if(k%3==0){
                        cambiarLSB(txt.charAt(i),red,j);
                    }
        			else if((k+1)==1||(k+1)==4||(k+1)==7){
                        cambiarLSB(txt.charAt(i),green,j);
                    }
                    else if((k+2)==2||(k+2)==5){
                        cambiarLSB(txt.charAt(i),blue,j);
                    }
                    Color nuevoColor = new Color(red,green,blue);
                    img.setRGB(x,y,nuevoColor.getRGB());
                    nPixel++;
        	    }
            }
            File outputfile = new File(imgNameResult);
            ImageIO.write(img, "png", outputfile);
        } 
        catch(FileNotFoundException ex){
            System.out.println("Archivo no encontrado");
        }
        catch (IOException ex) {
            System.out.println("No se pudo leer la imagen");
        }    
	} 


   public static String obtenerTxt(String fileName) throws FileNotFoundException, IOException{
	    FileReader f=new FileReader(fileName);
        StringBuilder sb = new StringBuilder();
        BufferedReader b= new   BufferedReader(f);
        try{
            
            int c=0;
            while((c=b.read())!=-1){
                sb.append((char)c);
            }    
        } 
        catch (IOException e) {
            System.out.println("Problemas al leer");
        }
		return sb.toString();
	}


}
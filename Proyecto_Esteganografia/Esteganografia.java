/**
 * Clase que implementa el proceso de Esteganografia 
 * ayudandose de dos clases, Ocultar para guardar el texto 
 * en una imagen dada, y Mostrar para develar el mensaje de
 * alguna imagen en la que ya se haya guardado un mensaje
 * Ademas que estas dos clases utilizan el metodo de LSB
 * @author Rossana Palma López
 * @version 1.0
 * */
public class Esteganografia{
	public static void main (String [] args){
		//Entrada por argumentos 
		String type=args[0];//Tipo de operacion a realizar
		String fileTxt=args[1];//Texto a guardar o archivo de texto 
		String imgFileName=args[2];//Imagen a procesar
		switch(type){//Switch segun el tipo de operacion
			case "O"://Ocultar
				String imgFileNameResult=args[3];
				Ocultar o = new Ocultar();//Instancia de la clase Ocultar
				o.codificar(imgFileName,fileTxt,imgFileNameResult);//Llamada al metodo codificar
			break;
			case "M"://Mostrar
				Mostrar m=new Mostrar();//Instancia de la clase Mostrar
				m.decodificar(imgFileName, fileTxt);//Llamada al metodo decodificar
		}

	}
}//Fin de la clase
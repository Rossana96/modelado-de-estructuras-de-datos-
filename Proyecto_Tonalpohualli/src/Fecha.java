/**
*Esta clase define el objeto Fecha con los datos de dia, mes y año
*y con sus respectivas operaciones, como saber que año es bisiesto
*y la diferencia de dias que hay de una fecha dada a enero del mismo
*año designado en la fecha.
*@author Rossana Palma López
*@version 1.0
*/
import java.util.Scanner;
public class Fecha{
	
    /*
    *Variables globales de la clase 
    */
	private String dia;//Cadena que representa el dia
	private String mes;//Cadena que representa el mes
	private String anio;//Cadena que representa el año


    /**
    *Constructor de la clase fecha, que define al objeto fecha
    *apartir de una cadena para el dia, otra para el mes y para el año
    *arroja una excepcion si al contruir la fecha recibe como parametros
    *las Fechas del 5 al 14 de octubre de 1582 o la fecha es incorrecta
    *@param d Cadena para el dia, 
    *@param m Cadena para el mes y 
    *@param a Cadena para el año 
    */
	public Fecha(String d, String m, String a)
		throws IncorrectDateException, NumberFormatException{
		this.dia=d;
		this.mes=m;
		this.anio=a;
        if(obtenerDia()>4&&obtenerDia()<15&&obtenerMes()==10&&obtenerAnio()==1582){
            throw new IncorrectDateException("●︿● Has ingresado mal la fecha pues nunca existio ≥⁰,⁰≤");
        }
        if(!fechaCorrecta()){
            throw new IncorrectDateException("●︿● Has ingresado mal la fecha, asegurate que la hayas ingresado correctamente");
        }



	}

    /**
    *Setter de la clase que asigna un dia a la fecha
    *@param d Una cadena que representa el dia de la Fecha
    */
    public void asignar_dia(String d){
        dia=d;

    }

    /**
    *Setter de la clase que que aigna un mes a la fecha
    *@param m Una cadena que representa el mes de la Fecha
    */
    public void asignar_mes(String m){
        mes=m;

    }

    /**
    *Setter de la clase que asigna un año a la fecha
    *@param a Una cadena que representa el año de la Fecha
    */
    public void asignar_anio(String a){
        anio=a;
    }

    /**
    *Getter de la clase que devuelve dia de la fecha
    *ademas que lo devuelve como entero, transformando 
    *la cadena a entero 
    *@return un entero que representa el dia de la fecha
    */
	public int obtenerDia() {
        int dia_num=Integer.parseInt(dia);
        return dia_num;
    }

    /**
    *Getter de la clase que devulve el mes de la fecha
    *toma los primeros 3 caracteres y realiza una seleccion
    *del entero que le corresponde a ese conjunto de caracteres
    *@return un entero que representa el mes de la fecha 
    */
    public int obtenerMes() {
        String mes_3char=mes.substring(0,3).toUpperCase();
        int mes_num=0;
        switch(mes_3char){
        	case "ENE":
        		mes_num=1;
        	break;
        	case "FEB":
        		mes_num=2;
        	break;
        	case "MAR":
        		mes_num=3;
        	break;
        	case "ABR":
        		mes_num=4;
        	break;
        	case "MAY":
        		mes_num=5;
        	break;
        	case "JUN":
        		mes_num=6;
        	break;
        	case "JUL":
        		mes_num=7;
        	break;
        	case "AGO":
        		mes_num=8;
        	break;
        	case "SEP":
        		mes_num=9;
        	break;
        	case "OCT":
        	 	mes_num=10;
        	break;
        	case "NOV":
        		mes_num=11;
        	break;
        	case "DIC":
        		mes_num=12;
        	break;
            default:
            break;				
        }
		return mes_num;
    }

    /**
    *Getter de la clase que devuelve el año de la fecha
    *ademas que lo devuelve en forma de entero, transformando
    *@return un entero que representa el año de la Fecha
    */    
    public int obtenerAnio() {
    	int anio_num=Integer.parseInt(anio);
        return anio_num;
    }

    /**
    *Metodo booleano que verifica si el año de la fecha dada 
    *es bisiesto o no, devuelve true si el residuo de dividir 
    *el año entre 4 es 0 y el año entre 100 el residuo es diferente
    *a 0, o si el residuo de dividir el año entre 400 es igual a 0
    *devuelve flase en caso contrario
    */
	private boolean anioBis(){
		return((obtenerAnio()%4==0)&&(obtenerAnio()%100!=0)||(obtenerAnio()%400==0));
	}

    /**
    *Metodo booleano que verifica que la fecha a construir sea
    *la correcta, verifica que el dia sea correcto, el mes y el año
    *devuelve true si el dia cumple esta dentro del rango de dias
    *que tiene el mes de la fecha y ser mayor o igual a 1,
    * asi para febrero, si el año es bisiesto febrero tine a los mas 29
    *si no los es tiene a lo mas 28 dias. Para los meses 4,6,9 y 11 el 
    *numero de dias maximo es 30, y para el resto de meses el numero de dias 
    *maximo sera 31.     
    *El mes debe estar en el rango del 1 al 12 y el año ser mayor a 0
    */
	private boolean fechaCorrecta(){
		boolean dia_Corecto, mes_Correcto, anio_Correcto;
        switch(obtenerMes()){
            case 2:
                if(anioBis()){
                    dia_Corecto=(obtenerDia()>=1&& obtenerDia()<=29);
                }
                else{
                    dia_Corecto=(obtenerDia()>=1&& obtenerDia()<=28);
                }
            break;
            case 4:
            case 6:
            case 9:
            case 11:
                dia_Corecto=(obtenerDia()>=1&&obtenerDia()<=30);
            break;
            default:
                dia_Corecto=(obtenerDia()>=1&&obtenerDia()<=31);        
            break;

        }
        mes_Correcto=(obtenerMes()>=1&&obtenerMes()<=12);
        anio_Correcto=(obtenerAnio()>0);
        return dia_Corecto && mes_Correcto && anio_Correcto;   
	}

    /**
    *Metodo booleano que verifica que el formato de la fecha a crear sea el correcto
    *devuelve true si el dia cumple con tener a lo mas 2 digitos,
    *si el mes cumple con tener como minimo 3 caracteres
    *y si el año cumple con tener exactamente 4 digitos
    */
	private boolean formatoCorrecto(){
		boolean dia_FormCorrect, mes_FormCorrect, anio_FormCorrect;
		dia_FormCorrect=(dia.length()<=2);
		mes_FormCorrect=(mes.length()>=3);
		anio_FormCorrect=(anio.length()==4);
        return dia_FormCorrect && mes_FormCorrect && anio_FormCorrect; 
	}

    private String quitarCeros(String a){
        String sin_ceros=a.replaceFirst("0","");
        return sin_ceros;
    }

    /**
    *toString de la clase que devuelve una cadena para imprimir
    *nuestro objeto de la clase
    *@return Una cadena que contiene el dia, mes y año de la fecha
    */
    public String toString(){

        return dia + " de " + mes + " de " + anio;
    }

    /**
    *Metodo qu realiza el calculo de cuantos dias de diferencia
    *hay entre el dia de la fecha dada al 1 de enero del mismo año de la fecha dada
    *@return un enetero que representa el numero de dias que hay
    *de enero del año de la fecha a el dia del mes de la fecha dada
    */
    public int diferenciaEnero(){
        int day=obtenerDia();
        int month=obtenerMes();
        int num_dias_trans=0;
        switch(month){
            case 1:
                if(day==1){
                    num_dias_trans=0;
                }
                if(day>1){
                    num_dias_trans=day;
                }
            break;
            case 2:
                num_dias_trans=31+day;
            break;
            case 3:
                if(anioBis()){
                    num_dias_trans=60+day;
                }
                else{
                    num_dias_trans=59+day;
                }
            break;
            case 4:
                if(anioBis()){
                    num_dias_trans=91+day;                    
                }
                else{
                    num_dias_trans=90+day;
                }
            break;
            case 5:
                if(anioBis()){
                    num_dias_trans=121+day;
                }       
                else{
                    num_dias_trans=120+day;
                }
            break;    
            case 6:
                if(anioBis()){
                    num_dias_trans=152+day;
                }         
                else{
                    num_dias_trans=151+day;
                }
            break;
            case 7:
                if(anioBis()){
                    num_dias_trans=182+day;
                }         
                else{
                    num_dias_trans=181+day;
                }
            break;
            case 8:
                if(anioBis()){
                    num_dias_trans=213+day;
                }         
                else{
                    num_dias_trans=212+day;
                }
            break;
            case 9:
                if(anioBis()){
                    num_dias_trans=244+day;
                }         
                else{
                    num_dias_trans=243+day;
                }
            break;      
            case 10:
                if(anioBis()){
                    num_dias_trans=274+day;
                }         
                else{
                    num_dias_trans=273+day;
                }
            break;
            case 11:
                if(anioBis()){
                    num_dias_trans=305+day;
                }         
                else{
                    num_dias_trans=304+day;
                }
            break;
            case 12:
                if(anioBis()){
                    num_dias_trans=335+day;
                }         
                else{
                    num_dias_trans=334+day;
                }
            break;
        }
        return num_dias_trans;

    }

}//Fin de la clase Fecha.java
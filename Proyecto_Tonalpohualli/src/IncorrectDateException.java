public class IncorrectDateException extends Exception{

	public IncorrectDateException(String mensaje){
		super(mensaje);
	}	
}
/**
*Clase que ayuda a calcular alguna fecha de nustro calendario (Gregoriano)
*en el calnedario azteca, o tambien conocido como TonalPohualli. En esta clase
*se pueden calcular las fechas desde el el 1 de enero del año 1 de la era cristiana
*es decir calcula la correlacion entre calendarios azteca y calendario juliano,
*asi como la correlacion del calendario azteca y calendario gregoriano, este implementado 
*en la reforma del calendario apartir del año 1582.
*Esta contiene operaciones tanto para calcular el numero juliano como para calcular el
*numero gregoriano asi como operaciones auxiliares para calcular numeros en distintos modulos.
*Este programa toma como referencia la correlacion de Caso.
*@author Rossana Palma López
*@version 1.0
*/
import java.lang.Math;
import java.util.Map;
import java.util.TreeMap;
import java.util.Scanner;
public class TonalPohualli{

	/*
	*Variable global de la clase
	*/
	String fecha_Tonalpohualli;//Cadena que representa la fecha en Nahuatl o segun el Tonalpohualli

	/**
	*Metodo que devuelve la fecha del Tonalpohualli para fechas antes de la reforma del calendario
	*es decir para fechas del calendario juliano.
	*@param a Objeto de la clase Fecha.
	*@param simb_dia Objeto de la clase Map( diccionario) que representa los simbolos en nahuatl para los dias
	*@param simb_anio Objeto de la clase Map (diccionario) que representa los simbolos en nahuatl para los años
	*@return Una cadena que contiene el dia segun el Tonalpohualli, llamando al metodo de la clase 
	*obtenerDiaTonalpohualli, y el año segun el Tonalpohualli, llamando al metodo obtenerAnioTonalpohualli 
	*/
	public String calcular_Juliano(Fecha a, Map<Integer , String> simb_dia, Map<Integer , String> simb_anio){
		fecha_Tonalpohualli="La fecha es: " + obtenerDiaTonalpohualli(((int)numeroJuliano(a)),simb_dia) + ", " + obtenerAnioTonalpohualli(((int)numeroJuliano(a)),simb_anio);
		return fecha_Tonalpohualli;
	}	

	/**
	*Metodo que devuelve la fecha del Tonalpohualli para fechas adespues de la reforma del calendario
	*es decir para fechas del calendario Gregoriano(el calendario de hoy en dia).
	*@param a Objeto de la clase Fecha.
	*@param simb_dia Objeto de la clase Map( diccionario) que representa los simbolos en nahuatl para los dias
	*@param simb_anio Objeto de la clase Map (diccionario) que representa los simbolos en nahuatl para los años
	*@return Una cadena que contiene el dia segun el Tonalpohualli, llamando al metodo de la clase 
	*obtenerDiaTonalpohualli, y el año segun el Tonalpohualli, llamando al metodo obtenerAnioTonalpohualli 
	*/
	public String calcular_Gregoriano(Fecha a, Map<Integer , String> simb_dia, Map<Integer , String> simb_anio ){
		fecha_Tonalpohualli="La fecha es: " + obtenerDiaTonalpohualli(numeroGregoriano(a),simb_dia) + ", " + obtenerAnioTonalpohualli(numeroGregoriano(a),simb_anio);
		return fecha_Tonalpohualli;
	}

	/**
	*Metodo estatico que calcula el numero Gregoriano de una fecha dada,esta fecha corresponde 
	*al calendario despues de la reforma, es decir al Calendario Grgoriano
	*es por eso que incluye la correccion de los dias.
	*Aparte esta se apoya obteniendo el "numero juliano",
	*restando al año de la fecha 1 y multiplicandolo por 365.25, que es el valor real del año juliano
	*y al numero obtenido sin decimal(si es que cuenta con este) se le suma la diferencia que hay 
	*de dias del 1 de enero de ese a la fecha dada, esto lo hace apoyandose en el metodo
	*diferenciaEnero de la clase Fecha, a esta diferencia se le resta uno para aplicar la correlacion de Caso.
	*y por ultimo se le restan los dias pues estos no fueron considerados en el calendario Gregoriano
	*a partir de la reforma.
	*@param a Objeto de la clase Fecha
	*@return entero que representa el numero Gregoriano   
	*/
	public static int numeroGregoriano(Fecha a){
		int num_Greg=0;
		double numJul=0;
		if(numeroJuliano(a)>=577748&&numeroJuliano(a)<=620618){
			numJul=((a.obtenerAnio()-1)*365.25);
			num_Greg=((int)numJul-10)+(a.diferenciaEnero()-1);

		}
		else if(numeroJuliano(a)>=620619&&numeroJuliano(a)<=657143){
			numJul=((a.obtenerAnio()-1)*365.25);
			num_Greg=((int)numJul-11)+(a.diferenciaEnero()-1);
		}
		else if(numeroJuliano(a)>=657145&&numeroJuliano(a)<=693668){
			numJul=((a.obtenerAnio()-1)*365.25);
			num_Greg=((int)numJul-12)+(a.diferenciaEnero()-1);
		}	
		else if(numeroJuliano(a)>=693669&&numeroJuliano(a)<=766718){
			numJul=((a.obtenerAnio()-1)*365.25);
			num_Greg=((int)numJul-13)+(a.diferenciaEnero()-1);
		}
		return num_Greg;
	}

	/**
	*Metodo auxiliar que devuelve la parte decimal de cualquier  dato  de tipo double
	*utiliza una variable entera auxiliar que representara la parte entera
	*del double que se tiene como parametro y devuelve un double que es igual a la diferencia
	*del double del parametro menos la parte entera de eses double
	*@param num variable double 
	*@return double que representa la parte decimal del parametro
	*/
	private double parteDecimal(double num){
		int part_Ent=(int)num;
		double part_Dec=num-part_Ent;
		return part_Dec;
	}

	/**
	*Metodo que calcula la combinacion numero-simbolica correspondiente al dia en el Tonalpohualli
	*Esto lo realiza apoyandose en el numero Juliano o el numero Gregoriano segun sea el caso.
	*La forma en la que realiza el calculo es la siguiente,primero le suma al numero juliano 61 
	*para obtenerla asociación inicial de un Tonalpohualli y  despues calcular el numero de series de 260 
	*que existen en el numero Juliano mas 61, esto lo podiramos ver como el numero Juliano en modulo 260
	*para esto nos ayudamos dl metodo de la clase parteDecimal.
	*El numero obtenido sera la cifra del Tonalpohualli correspondiente al numero Juliano
	*A partir de esta cifra obtendremos el la combinacion numero-simbolica.
	*El numero lo obtenemos calculando el numero en modulo 13(de nuevo auxliandonos del metodo parteDecimal)
	*13 ya que los numerales posibles estan en un rango de enteros del 1 al 13.
	*El simbolo se obtinene calculando el numero en modulo 20, 20 por que el numero de simbolos posibles
	* estan en un rango de 1 al 20 (distintos).
	*Los simbolos seran encontrados en un diccionario (Objeto de la clase Map) nuestra clave sera el numero
	*obtnenido al calcular modulo 20.
	*Por ultimos asignamos a una cadena a devolver la combinacion numero-simbolica para el dia.
	*@param n entero que corresponde al numero juliano o gregoriano de nuestra fecha
	*@param simb_dia Objeto de la clase Map donde se encontraran los simbolos posibles para el dia.
	*@return cadena que representa la combinacion numero-simbolica para el dia en el Tonalpohualli.
	*/
	private String obtenerDiaTonalpohualli(int n, Map<Integer , String> simb_dia){
		double series260=(double)(n+61)/260;
		double parte_deci_Series=parteDecimal(series260);
		int cifraTonal=(int)(parte_deci_Series*260);
		double serie13=(double)cifraTonal/13;
		int numero=(int)Math.round((parteDecimal(serie13)*13));
		double serie20=(double)cifraTonal/20;
		int num_simbolo=(int)Math.round((parteDecimal(serie20)*20));
		String simboloDia =simb_dia.get(num_simbolo);
		String dia_Tonalpohualli=numero+"-"+simboloDia;
		return dia_Tonalpohualli;
	} 
	
	/**
	*Metodo que calcula la combinacion numero-simbolica correspondiente al año en el Tonalpohualli
	*Esto lo realiza apoyandose en el numero Juliano o Gregoriano segun sea el caso.
	*La forma en que realiza el calculo es la siguiente, primero al numero juliano o gregoriano
	*se le suma 309 dias, pues corresponden a los dias anteriores a la era cristiana en los cuales
	*comenzaba un fuego nuevo(siglo azteca), despues al numero obtenido lo dividimos entre 365 para 
	*obtener la cantidad de años segun los aztecas. Si el producto obtenido no tiene residuo este
	*se considera como el numero de años transcurridos desde el primer fuego nuevo hasta la fecha dada
	*si tiene residuo (es decir decimal) entonces el numero a considerar es el siguiente entero
	*esto se expresa ayudandonos con el metodo ceil(techo de un numero) de la clase Math de java
	*Despues de comprobar esto obtenemos el numero en modulo 52(utilizamos el metodo parteDecimal) 
	*52 por que es el numero de años que tiene un siglo azteca o fuego nuevo.
	*El numero de año lo obtenemos calculando el numero anteriormente obtenido en modulo 13(auxiliandonos
	*del metodo parteDecimal) + 1(pues los numerales empiezan en 2), y 13 ya que los numerales posibles estan en un rango de enteros del 1 al 13.
	*El simbolo se obtinene calculando el numero en modulo 4, 4 por que el numero de simbolos posibles
	* estan en un rango de 1 al 4 (distintos).
	*Los simbolos seran encontrados en un diccionario (Objeto de la clase Map) nuestra clave sera el numero
	*obtnenido al calcular modulo 4.
	*Por ultimos asignamos a una cadena a devolver la combinacion numero-simbolica para el año.   
	*@param n entero que corresponde al numero juliano o gregoriano de nuestra fecha
	*@param simb_dia Objeto de la clase Map donde se encontraran los simbolos posibles para el año.
	*@return cadena que representa la combinacion numero-simbolica para el dia en el Tonalpohualli
	*/
	private String obtenerAnioTonalpohualli(int n, Map<Integer , String> simb_anio){
		double anio_serieTolteca=Math.ceil(((double)n+309)/365);
		double serie52=anio_serieTolteca/52;
		int marc_Serie52=(int)Math.round(parteDecimal(serie52)*52);
		double numAnio_serie13=(marc_Serie52/13.0);
		int numAnio_Tol=(int)Math.round((parteDecimal(numAnio_serie13)*13)+1);
		double numAnio_serie4=(double)marc_Serie52/4;
		String simboloAnio=simb_anio.get((int)(parteDecimal(numAnio_serie4)*4));
		String anioTonalpohualli = numAnio_Tol + "-" + simboloAnio;
		return anioTonalpohualli;
	}


	/**
	*Metodo estatico que calcula el numero Juliano, es decir el numero
	*de dias que han transcurrido desde el inicio de la era cristiana hasta la fecha
	*que estamos buscando, si es  que esta esta comprendida entre las fechas antes de la 
	*reforma de 1582, es decir el Calendario Gregoriano.
	*L forma de realizar el calculo es: Primero se le resta uno al año
	*de la fecha dada, esto por que calendario Juliano no tuvo año cero
	*, despues este numero obtenido se le multiplica 365.25, valor del año Juliano
	*y a este producto se le suma los dias que hay a partir del 1 de enero
	*del año de la fecha dada, hasta el dia de la fecha dada.
	*@param a Objeto de la clase Fecha
	*@return un entro que representa el numero Juliano
	*/
	public static int numeroJuliano(Fecha a){
		int anio_exact=a.obtenerAnio()-1;
		double dias_trans_juliano=anio_exact*365.25;
		int numeroJuliano = (int)dias_trans_juliano + (a.diferenciaEnero()-1);
		return numeroJuliano; 

	}

	/*
	*Main de la clase TonalPohualli.java
	*/
	public static void main(String [] args){
		
		/*
		*Se crea un objeto de la clase Map utilizando la implementacion TreeMap, 
		en este estraran los simbolos para los dias (cadenas de caracteres) 
		con su respectiva clave que es un entero.
		*/
		Map<Integer, String> simbolos_dia = new TreeMap< Integer, String>();
		simbolos_dia.put(1,"Cipactli(caimán)"); 		simbolos_dia.put(2, "Ehecatl(viento)");
		simbolos_dia.put(3, "Calli(casa)");			simbolos_dia.put(4, "Cuetzpalli(lagartija)");
		simbolos_dia.put(5, "Coatl(serpiente)");			simbolos_dia.put(6, "Miquiztli (muerte)");
		simbolos_dia.put(7, "Mazatl(venado)"); 			simbolos_dia.put(8, "Tochtli(conejo)");
		simbolos_dia.put(9, "Atl(agua)"); 			simbolos_dia.put(10, "Itzcuintli(perro)");
		simbolos_dia.put(11, "Uzumatli(mona)"); 		simbolos_dia.put(12, "Malinalli(hierba)");
		simbolos_dia.put(13, "Acatl(carrizo)");			simbolos_dia.put(14, "Ocelotl(ocelote)");
		simbolos_dia.put(15, "Cuauhtli(águila)");		simbolos_dia.put(16, "Cuscacuauhtli(águila real)");
		simbolos_dia.put(17, "Ollin(movimiento)");			simbolos_dia.put(18, "Tecpatl(pedernal)");
		simbolos_dia.put(19, "Quiahuitl(lluvia)"); 		simbolos_dia.put(20, "Xuchitl(flor)");
		/*
		*Se crea un objeto de la clase utilizando la implementacion TreeMap, 
		en este estaran los simbolos para los dias (cadenas de caracteres) 
		con su respectiva clave que es un entero. 
		*/
		Map<Integer , String> anio_nahuatl = new TreeMap<Integer, String>();
		anio_nahuatl.put(1, "Acatl(carrizo)"); 			anio_nahuatl.put(2, "Tecpatl(pedernal)");
		anio_nahuatl.put(3, "Calli(casa)");			anio_nahuatl.put(4, "Tochtli(conejo)");
		Scanner in = new Scanner(System.in);//Se crea un objeto de Scanner para leer entrada estandar
        Fecha miFecha;//Se crea un Objeto de la clase Fecha sin inicializar
        TonalPohualli miFechaTonal=new TonalPohualli();//Se crea un objeto de la clase
        System.out.println("ヅ Calculadora de Fecha en Tonalpohualli ヅ");
        String day;//Cadena que representa el dia para la Fecha
        String month;//Cadena que representa el mes para la Fecha 
        String year;//Cadena que representa el año para la Fecha
        System.out.println("Ingresa el dia con numero (a dos digitos)");
        day=in.nextLine();//Se inicializa la variable con una entrada valida
        System.out.println("Ingresa el mes con letra (minimo 3 caracteres)");
        month=in.nextLine();//Se inicializa la variable con una entrada valida
        System.out.println("Ingresa el año con numero (a 4 digitos)");
        year=in.nextLine();//Se inicializa la variable con una entrada valida
        String tonal="";//Cadena que representa nuestra fecha de salida
        try{//Se entra a un try pues se puede generar una excepcion
        	Fecha baseJul= new Fecha("4","oct","1582");//Fecha limite para el calendario Juliano
        	Fecha baseGreg= new Fecha("15","oct","1582");//Fecha donde inicia el calendario Gregoriano
            miFecha=new Fecha(day,month,year); //Inicializacion del Objeto Fecha
            //Si la Fecha construida es menor al limite del calendario Juliano
            if(numeroJuliano(miFecha)<=numeroJuliano(baseJul)){  
            	//Llamamos al metodo de la clase calcularJuliano
            	tonal=miFechaTonal.calcular_Juliano(miFecha, simbolos_dia, anio_nahuatl);
            }
            //Si la fecha construida es mayor al limite del calendario Gregoriano
            else if(numeroJuliano(miFecha)>=numeroJuliano(baseGreg)){
            	//Llamammos al metodo de la clse calcularGregoriano
            	tonal=miFechaTonal.calcular_Gregoriano(miFecha, simbolos_dia, anio_nahuatl);	
            }
            //Salida del programa
        	System.out.println(tonal);
        }
        //Si se porduce alguna excepcion si se produce
        catch(IncorrectDateException e){
            System.out.println(e);        
        }
        catch(NumberFormatException e){
            System.out.println("●︿● El dia y el año deben ingresarse con numero vuelve a ingresar la fecha");
        }

		
        
	}
}//Fin de la clase Tonalpohualli.java



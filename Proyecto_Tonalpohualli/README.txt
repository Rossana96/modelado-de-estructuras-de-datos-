------------README PROYECTO 01----------------
Las siguientes clases presentes ayudan a calcular el Tonlpohualli de una fecha dada por entrada, y para manejar estas fechas por eso se incluye la clase Fecha que ayuda a construir una fecha d manera correcta.
Se incluye tambien uan clase que extiende a la interfaz Ecxepcion, es decir creamos la clase para atrapar la excepcion que se produce cuando la fecha es incorrecta.
Por ultimo tenemos la clase principal TonalPohualli donde tambien esta el Main, con esta clase se calcula la fecha segun el Tonalpohualli. La salida es la fecha correspondiente en el Tonalpohualli. Esta salida es igual a la que se obtiene en la pagina de http://www.azteccalendar.com/ en la configuracion segun Alfonso Caso.

La forma de compilar es la siguiente:
Situados en la carpeta src compilamos desde consola con
	javac -encoding utf8 *.java

La forma de ejecutar el progra es la siguiente
	java TonalPohualli 

	enseguida nos aparecera lo siguiente

	? Calculadora de Fecha en Tonalpohualli ?
	Ingresa el dia con numero (a dos digitos)

	se debera ingresar tal y como marca la instruccion en caso contarrio se arrojara una excepcion

	luego aparecera esto 

	Ingresa el mes con letra (minimo 3 caracteres)

	de igual forma se debe de ingresar tal y como se marca o arrojara uan excepcion

	y por ultimo 

	Ingresa el año con numero (a 4 digitos)

la salida sera la correspondencia de la fecha ingresada en el Tonalpohualli

Las clases se realizaron y compilaron en el sistema operativo Windows 8.1	

------------------------------------------------------
Datos 

Nombre Rossana Palma López 
N° cuenta 312047890
